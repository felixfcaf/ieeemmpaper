% Also note that the "draftcls" or "draftclsnofoot", not "draft", option
% should be used if it is desired that the figures are to be displayed in
% draft mode.
%
%\documentclass[journal, draftcls, onecolumn]{IEEEtran}
\documentclass[journal]{IEEEtran}

%\usepackage{algorithm}
%\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{amsmath,amssymb}
\usepackage{url}
% *** SUBFIGURE PACKAGES ***
\ifCLASSOPTIONcompsoc
  \usepackage[caption=false,font=normalsize,labelfont=sf,textfont=sf,subrefformat=parens,labelformat=parens]{subfig}
\else
  \usepackage[caption=false,font=footnotesize,subrefformat=parens,labelformat=parens]{subfig}
\fi

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
% can use linebreaks \\ within to get better formatting as desired
% Do not put math or special symbols in the title.
\title{The Green Metadata Standard for Energy-Efficient Video Consumption}

\author{
  Felix~C.~Fernandes,
  Xavier~Ducloux,
  Zhan~Ma,
  Esmaeil~Faramarzi,
  Patrick~Gendron
  and~Jiangtao~Wen.
\thanks{F. C. Fernandes and E. Faramarzi are with Samsung Research America, Dallas. X. Ducloux and P. Gendron are with
  Thomson Video Networks, France. Z. Ma is with  Huawei Technologies, USA. J. Wen is with Tsinghua University, China}% <-this % stops a space
}

% make the title area
\maketitle

% the example papers don't have abstracts, so none here either.

\IEEEpeerreviewmaketitle

\IEEEPARstart{I}{f}  you use your mobile device for video applications, then you probably wish that your mobile battery charge could last
longer. Given the current popularity of video applications such as streaming (e.g., YouTube, Netflix), conferencing
(e.g., Skype, Google Hangout) and  sharing (e.g., Google+, Facebook), there is already a huge demand for increased
battery life on mobile devices. This demand will increase significantly because of  the anticipated
69.1\%  annual growth rate of mobile video traffic from 2013 to 2018 \cite{Cisco}. Unfortunately, innovations in battery
research are unlikely to satisfy the demand for longer battery life during mobile video consumption. The only solution
is to provide energy-efficient video consumption. 

In April 2013, MPEG issued a Call for Proposals (CfP) on energy-efficient video consumption. The CfP responses showed
that, without any loss in the Quality of Experience (QoE), metadata can be used to reduce power consumed during the encoding,
decoding, display and selection of video. Furthermore, when battery levels are critically low, consumers may use
the metadata to trade-off between QoE and larger power savings. Based on compelling evidence from the CfP responses, MPEG initiated
standardization of Green Metadata  \cite{GreenMetadata}  for energy-efficient video consumption. This article describes
how metadata enables large power reductions when QoE is maintained and even larger reductions when QoE is allowed to
change. When QoE is maintained, metadata enables average power reductions of 12\%, 12\% and 26\% respectively during
encoding, decoding and display. In addition, we measured up to 80\% power savings at lowered, but acceptable, QoE levels.
Section~\ref{secn:funcArch} 
describes the functional architecture of a system that exploits the Green Metadata standard for energy-efficient media
consumption. Subsequent sections refer to this functional architecture to explain power reductions in various components
of the system. 

\section{A Functional Architecture for Green Metadata}
\label{secn:funcArch}
Figure \ref{fig:green_mpeg_system} shows the functional architecture for a transmitter/receiver system that uses
Green Metadata which is generated during pre-processing and/or encoding at the transmitter. The metadata is then
multiplexed with or encapsulated into a bitstream that is delivered to the receiver. The metadata extractor sends the
Green Metadata to a power-optimization module that interprets it and then applies controls to reduce power consumption
during decoding and display of the video.  
% Next sentence is optional.
In addition, the receiver's power-optimization module can collect
receiver information, such as remaining battery capacity, and send it to the transmitter as Green Feedback that adapts
the encoder operations for power-consumption reduction.   

\begin{figure}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=\columnwidth]{figures/mediaSelection/figureFunctionalArchitecture-v3.pdf}\\
  \caption{Functional architecture of a system that uses Green Metadata }\label{fig:green_mpeg_system}
\end{figure}

\section{Display Power Reduction}
\label{secn:display}
Display units in modern electronic devices consume large amounts of power. Therefore, display power reduction is a
critical problem, especially for mobile devices with limited battery charges. The \emph{Display Adaptation (DA)} technique (also known as \emph{backlight dimming}) 
\cite{Cheng2004, Huang2013} has been used to reduce display power consumption. However, when applied to video content,
DA produces flicker artifacts. In this section, we explain how the metadata in the Green Metadata standard can be used to
mitigate flicker and reduce power significantly when DA is applied to video content on mobile devices. 

\subsection{How does DA work?}
The luminance of light perceived from a Liquid Crystal Display (LCD) or an Organic Light-Emitting Diode (OLED) display
is not only dependent on the transmittance of pixels (RGB values), but is also proportional to the backlight intensity of the LCD
panel (or the supply voltage of the OLED panel). Since the power consumption changes negligibly with variations of RGB
values, DA can be utilized to reduce the power consumption considerably without sacrificing the quality by 
dimming the LCD backlight and simultaneously scaling up the RGB values proportional to the dimming level. 

\begin{figure*}%
    \subfloat[\label{subfig:DA1}]{\frame{\includegraphics[width=0.325\linewidth]{figures/DA/DA01.png}}}
    \hfill
    \subfloat[\label{subfig:DA2}]{\frame{\includegraphics[width=0.325\linewidth]{figures/DA/DA02.png}}}
    \hfill
    \subfloat[\label{subfig:DA3}]{\frame{\includegraphics[width=0.325\linewidth]{figures/DA/DA03.png}}}
    \\[-8pt]
    \subfloat[\label{subfig:DA4}]{\frame{\includegraphics[width=0.325\linewidth]{figures/DA/DA04.png}}}
    \hfill
    \subfloat[\label{subfig:DA5}]{\frame{\includegraphics[width=0.325\linewidth]{figures/DA/DA05.png}}}
    \hfill
    \subfloat[\label{subfig:DA6}]{\frame{\includegraphics[width=0.325\linewidth]{figures/DA/DA06.png}}}
    \vspace{-3pt}\caption{\protect\subref{subfig:DA1} Video frame under full backlight. \protect\subref{subfig:DA2} Backlight is reduced by 26\%, which results in contrast reduction. \protect\subref{subfig:DA3} Result of applying DA. \protect\subref{subfig:DA4} Backlight is severely reduced by 65\%. \protect\subref{subfig:DA5} Result of DA without optimal dynamic range adjustment. \protect\subref{subfig:DA6} Result of DA with contrast enhancement using dynamic range adjustment.}%
    \label{fig:DA}%
\end{figure*}

Figure \ref{fig:DA} provides an example application of DA. Figure~\subref*{subfig:DA1} shows a video frame displayed
under the default full backlight. In Figure~\subref*{subfig:DA2}, the LCD backlight is reduced by 26\%. This reduces
display power consumption by 26\%; however, the image contrast is also reduced significantly. In
Figure~\subref*{subfig:DA3}, image quality is almost completely restored by using DA to scale up the RGB
values. Depending on the amount of backlight reduction, some high RGB values may be saturated to the 
maximum level (255 for a 24-bit RGB image). 
For larger power savings, in Figure~\subref*{subfig:DA4} the backlight is reduced by 65\%. With
this huge backlight reduction, simply scaling up the RGB values does not restore image quality, as seen in
Figure~\subref*{subfig:DA5}. Fortunately, contrast enhancement can be applied  within a dynamic range that contains a
majority of the RGB values \cite{Cheng2004}. Figure~\subref*{subfig:DA6} shows significant improvement compared to
Figure~\subref*{subfig:DA5}, after applying the aforementioned contrast enhancement.

\subsection{How does the Standard use Metadata for DA?}
Previous studies on DA focused exclusively on static images. Unfortunately, these techniques have limited
applicability in realistic scenarios involving video content. First, when the techniques are applied to video
sequences, flicker artifacts occur due to large backlight variations from dramatic changes in image characteristics at
scene changes. A second limitation of the previous studies is that a significant latency (in milliseconds) exists
between the instant when the backlight scaling control is applied and the 
instant when the backlight actually changes in response to the control. Therefore, the backlight values
will not be synchronized with the displayed frames and flickering is inevitable. A third practical limitation is that
previous studies require DA to be applied successively to all video frames. However, existing displays cannot adjust the
backlight for each video frame because the backlight cannot be adjusted at the video frame rate: the interval between
backlight changes must be larger than the inter-frame interval. 

The Green Metadata Standard overcomes these limitations by signaling RGB-statistics, associated quality-level indicators and
dynamic-range bounds as metadata. Backlight settings that will result in the indicated quality
levels are derived from the RGB statistics. The dynamic-range bounds use contrast enhancement to improve
perceived quality. For large(small) power reduction, the receiver's power-optimization module will select the backlight setting
derived from RGB statistics associated with  a lower(higher) quality level. The third limitation described above is
overcome by aggregating the RGB statistics and associated quality levels over the interval between backlight
changes. The second limitation is overcome by transmitting the metadata with sufficient lead time relative to the video
frames so that the latency is not an issue. The first limitation is avoided by temporally smoothing the metadata so that
large variations are absent. 

To test DA power reduction, we used the Galaxy Tab~2 platform powered by the Monsoon power monitor which replaces the
tablet's battery and logs instantaneous power consumption of the tablet. Over a test set of 12 video sequences, we
computed an average power reduction of 26.3\% from metadata-assisted  DA, without using contrast enhancement. When the
metadata included the dynamic-range bounds that 
enable contrast enhancement, we measured up to 80\% power reduction at acceptable quality levels. Note that these
reductions are relative to the power consumption of the device and are not limited to the display unit within the
device. Further details are in \cite{m30484, m32480}. 

\section{Decoder Power Reduction}
\label{secn:decoder}
The Dynamic Voltage/Frequency Scaling (DVFS) power-reduction technique  has been extensively studied \cite{rabaey-bk} and implemented in various systems.
Intuitively, the dynamic power of a CMOS circuit is related linearly to the clock frequency $f$, and quadratically to the supply
voltage $V$, so that  $P \propto f \cdot V^2$. Because $V$  is a generalized power function of $f$, the power consumption may be
expressed as
\begin{equation}
\label{eqn:powerFreq}
P \propto f^{2+\alpha}. 
\end{equation}
Therefore, lowering the frequency reduces power consumption. For decoders that run on a CPU, we may apply Equation~\ref{eqn:powerFreq} to control the CPU frequency and thus reduce power. However, if the CPU frequency is lowered
arbitrarily, then decoding of complex pictures may not complete within frame-rate deadlines. Consequently, the ability 
to accurately predict the complexity of upcoming pictures is critical~\cite{Zhihai_PRQ}. Instead of using additional,
complicated modules to predict the complexity, the Green Metadata standard adopts a new approach to this problem: metadata
that indicates picture-decoding complexity is embedded in the bitstream and used by the receiver to set the CPU frequency at the lowest 
frequency that guarantees decoding completion within frame-rate deadlines and also provides the maximum power
reduction. 
\subsection{Using Metadata to Indicate Decoding Complexity}
Since the characteristics of video content usually change over time,  picture decoding complexity also does vary. Therefore, an
efficient protocol for complexity signaling is to specify the period over which complexity is constant and to 
provide Complexity Metrics (CMs) that indicate the decoding complexity within that period. When content characteristics
change, a new period is signaled along with accompanying CMs. The standard uses {\bf period\_type} to
signal the type of upcoming period over which  the CMs apply. The period\_type is either a single picture, a Group of
Pictures (GOP) or a time interval. 

The total decoding complexity is decomposed \cite{Zhan_TMM_CompModel} according to the complexity of six,
individual, Decoding Modules (DMs): (1) entropy decoding, (2) dequantization and inverse transform, (3) intra
prediction, (4) motion compensation, (5) deblocking, and (6) side-information  preparation (e.g., memory copies).
 The complexity of each DM is a function of the complexity for a particular operation, which is typically expressed in
 cycles, as well as the number of times that the operation must be  invoked in the specified
 period. Note that the CM for a DM is the  number of times that the DM's underlying operation is invoked within the
 specified period. The Green Metadata standard specifies a CM for each of the six DMs and embeds these CMs in
 Supplemental Enhancement Information (SEI) messages within an H.264/AVC bitstream

\subsection{Experimental Results for CM-Assisted DVFS Decoding}
To determine the power reduction from CM-assisted DVFS decoding, we used the Google Nexus 7 platform powered by the
Monsoon power monitor which replaces the tablet's battery and logs instantaneous power consumption of the tablet. For
on-platform bitstream playback we used the software decoder in the RockPlayer application \cite{rockplayer}. 
Our measurements showed that CM-assisted DVFS decoding provides an average power reduction of 12.5\% over a test set of
11 bitstreams on the Google Nexus 7 platform. Note that this reduction is relative to the power consumption of the
device and is not limited to the decoder module within the device. Further details are in \cite{m30484}. 



\section{Encoder Power Reduction}
An encoder can achieve power reduction by encoding alternate high-quality and low-quality segments, in a segmented
delivery mechanism such as DASH \cite{xavier-4-dash, xavier-5-iraj}. The power reduction occurs because low-complexity encoding mechanisms (fewer encoding
modes, fewer reference pictures, smaller search ranges, etc.) are used to produce the low-quality segments. A metric
describing the quality of the last picture of each segment is delivered as metadata to the decoder. This section
describes how Cross-Segment Decoding  (XSD) \cite{wencross} can be used to improve the quality of the low-quality segments. 
An XSD-enabled decoder will utilize quality metrics contained in the high-quality segments (from high complexity
encoding) to enhance decoding of the low-quality segments (from low complexity encoding), producing a visual experience
with significantly higher QoE, but with reduced average encoding complexity (and therefore reduced encoding power
consumption). Note that the decoding complexity for the first picture in the low-quality segment is increased, while the
decoding complexity for the other pictures remains the same as that for H.264/AVC-compliant decoders. 

\subsection{Cross Segment Decoding of Varying Quality Video}
At the transition from a segment with higher video quality to a temporally neighboring segment that is
encoded independently of the good quality segment and whose video quality is poorer, we term the last picture (in
display order) in the higher quality segment a ``Good Picture'' (GP),  the first IDR picture of the poor quality segment the
``Start Picture'' (SP), and the output from the current algorithm the ``Fresh Start'' (FS). Note that the SP as an IDR
picture was encoded without referencing the GP or any other frames in the higher quality segment. 

The PSNR values of the GP and SP are embedded as Green Metadata in H.264/AVC SEI messages. When an XSD-enabled decoder
receives this metadata, it will use the PSNR values to determine whether to apply an enhancement algorithm to enhance
the SP. Specifically, the decoder can use information contained in the GP to improve the quality of the decoded SP to
get FS, an improved reference frame for subsequent frames in the low quality segment. Once the decoder identifies the correspondences of the 
similarities between the two pictures, it can use the high quality areas in the GP to improve the quality
of the corresponding areas in the SP. % Next two sentences are optional.
Depending on the motion level in spatial regions of the SP, the decoder may use different enhancement algorithms, one
for relatively low motion areas, the other for the higher motion areas. For both algorithms, the decoder will look for
matches between areas in the decoded GP and the SP. 

\subsection {Experimental Results for Encoder Power Savings with XSD}
To demonstrate metadata-based power savings, we used the x264 AVC encoder installed on a Galaxy Tab~3.  XSD
modifications were applied to the JM 18.5 version of the AVC decoder. Power usage, was measured using the Monsoon power monitor
which replaces the battery and logs instantaneous power consumption in the aforementioned tablet. Across 10 test
sequences, an average of $12.1\%$ power reduction was measured on the tablet. When compared with H.264/AVC-compliant
decoding, the average PSNR, after XSD decoding, was improved by 0.2~dB. Further details are provided in 
\cite{m32439}. 


\section{Energy-Efficient Media Selection}
Over the past decade, delivery of multimedia over the Internet has become increasingly popular. The first commercial deployments were
based on proprietary streaming platforms such as Apple's HTTP Live Streaming \cite{xavier-1-apple}, Microsoft's Smooth
Streaming \cite{xavier-2-smooth} and Adobe's HTTP Dynamic Streaming \cite{xavier-3-adobe}. In 2011, MPEG standardized
Dynamic Adaptive Streaming over HTTP (DASH) \cite{xavier-4-dash}, \cite{xavier-5-iraj}, the first open standard in this field.
All these solutions are based on client-side content selection based on network bandwidth monitoring: the client device
selects the appropriate video and audio representations at each switching point as a function of the input
bandwidth. The Green Metadata standard adds a new dimension to dynamic adaptive streaming: besides bandwidth
considerations, media selection can be based on the media's power consumption impact on the
decoder and the display of the client device. 

When the power resource is constrained, three media-selection strategies are possible:
\begin{enumerate}
\item Select a media representation that will require low power consumption. This leads to poor  QoE because the lower
  resolution or lower bitrate representations will be selected. 
\item For the next segment, select the most appropriate media representation based on the power consumption measured
  in the previous media segment. For example, on complex scenes (motion, detailed textures), the client device will select a
  representation with a lower resolution, while on simple scenes, it can switch back to a higher resolution. The issue
  is that the client device will react with at least one segment delay when the video-content complexity changes
  significantly. It can lead to visible changes of perceived quality, particularly for simple scenes when the resolution
  is increased.
\item Use Green Metadata to select the most appropriate representation pro-actively by
  anticipating any change in the video content. This will guarantee the best QoE for a desired
  power-consumption level.
\end{enumerate}

To enable the third strategy, the Green Metadata standard defines two types of metadata: decoder-power indication metadata and
display-power indication metadata. These metadata are associated to available video representations for each segment of
a media stream.
The available metadata representations will be signaled in a specific adaptation set within the Media
Presentation Description (MPD) file, which describes a manifest of available content in DASH. The association of a
metadata representation with a media representation is made in the MPD through the @associationId and
@associationType attributes. A metadata segment and its associated media segment(s) are time aligned on segment
boundaries, as shown in Figure~\ref{fig_sim}.

\begin{figure}[htp]
\centering
\includegraphics[scale=0.5]{./figures/mediaSelection/GreenMetadataForDASH-v3.pdf}
\caption{One metadata representation for one media representation}
\label{fig_sim}
\end{figure}


The decoder-power indication metadata gives the potential decoder power saving of each available representation of a
video segment. The potential power savings for a representation are indicated as two ratios. The first is the ratio of
the representation's Decoding-Operations Reduction (DOR) relative to the Decoding Operations (DO) of the most demanding
representation in the current segment. The second is the ratio of the representation's DOR relative to the DO of the
same representation in the previous segment. To determine the potential display power saving for a video segment, the
display-power indication metadata consists of RGB statistics and the resulting quality levels as explained in
Section~\ref{secn:display}. 

The client device first determines the average acceptable power consumption based on its remaining battery life and the total
duration of the video (parameter provided in the manifest file in case of on-demand content or requirements of duration
expressed by the user). For each switching point, using the above-mentioned metadata and power consumption measured in
the last segments, the player can define the best power-saving allocation strategy: the particular representation it
needs to download and the appropriate RGB statistics that will provide an acceptable quality level and the
desired power saving for that representation.

Additionally, if available in the video stream, complexity metrics, as defined in Section~\ref{secn:decoder}, can provide pro-active DVFS
processor control for optimal energy saving. RGB statistics, as defined in Section~\ref{secn:display}, can be used in addition
to reduce display power consumption. 



\section{Conclusion}
Because of the increasing popularity of video consumption on mobile devices, battery-powered systems that implement the functional architecture in
Figure~\ref{fig:green_mpeg_system}  are ubiquitous. Algorithmic, systemic and modular optimizations are key to
ensuring that such systems have low-power consumption. However, huge, additional power savings are achievable by 
considering the impact of the video content itself on the power consumption of various components within the functional
architecture. The Green Metadata standard embraces this philosophy and specifies the abstraction of the power-consumption determinants of
video content into a small amount of  metadata that accompanies the video bitstream and that enables the additional power
reductions to be achieved.

For display power reduction, the display-adaptation technology reduces power consumption by lowering the backlight (or voltage)
control of the display. The power-consumption determinants are the RGB statistics, corresponding quality levels and
dynamic-range bounds  which enable backlight settings to be derived for the specified quality levels. These determinants
are extracted from the video content at the transmitter, processed to avoid  inducing flicker and then encapsulated
in the bitstream as metadata. 

For decoder power reduction, the dynamic voltage/frequency scaling technology reduces power consumption by lowering CPU
frequency. The power-consumption determinants are the complexity metrics that indicate picture-decoding complexity so
that the CPU frequency is set to the lowest frequency that guarantees decoding completion within frame-rate deadlines
and also provides the maximum power reduction. These complexity metrics are extracted during video encoding and then
encapsulated in the bitstream as metadata.

For encoder power reduction, the Cross-Segment Decoding (XSD) technology enhances quality at the decoder after low-power
encoding is used at the transmitter. The determinant is a quality metric that is used by the XSD-decoder to enhance the
quality. The quality metric is computed at the transmitter and encapsulated in the bitstream as metadata.

For energy-efficient media selection, the power-consumption impact on the client's decoder and display is considered
when media is selected at each switching point. The power-consumption determinants are display-power indicators and
ratios of decoding-operations reduction relative to certain representations in the current and previous segments. These
determinants are computed at the transmitter and signaled in a specific adaptation set within the Media Presentation
Description file in DASH.

The Green Metadata Standard is expected to become an international standard in February 2015. Given the huge demand for
increased battery life on mobile devices, it is anticipated that an ecosystem will evolve to support the deployment of
the standard. The ecosystem would consist of entitities that generate, multiplex, distribute and finally apply the
metadata for power reduction. The French GreenVideo project \cite{greenVideo} is currently considering deployment of the
standard. 





\section*{Acknowledgment}
The work of Zhan Ma was performed when he was a Senior Standards Engineer at Samsung Research America, Dallas.
The work of Jiangtao Wen was partially supported by the National Science Foundation
for Distinguished Young Scholars of China (Grant No. 61125102), the State Key Program of
National Natural Science of China (Grant No. 61133008) and the National Significant Science and Technology
Projects of China (Grant No. 2012ZX01039-001-003-2). 


\bibliographystyle{./bibtex/IEEEtran}
\bibliography{./bibtex/IEEEabrv,./bibtex/IEEEexample}

\end{document}


